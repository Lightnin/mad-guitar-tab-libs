$(function() {

  // hide the story from view
  $("#story").hide();

  // ---- event handler ---- //
  $("#btn-click").click(function(e) {

    e.preventDefault()

    // grab the values from the input boxes, then append them to the DOM
    $(".worked").empty().append($("input.worked").val());
    $(".rain").empty().append($("input.rain").val());
    $(".driven").empty().append($("input.driven").val());
    $(".snow").empty().append($("input.snow").val());
    $(".drunk").empty().append($("input.drunk").val());
    $(".dirty").empty().append($("input.dirty").val());
    $(".willin").empty().append($("input.willin").val());

    $(".road").empty().append($("input.road").val());
    $(".pretty").empty().append($("input.pretty").val());
    $(".alice").empty().append($("input.alice").val());
    $(".tucson").empty().append($("input.tucson").val());
    $(".tucumcari").empty().append($("input.tucumcari").val());
    $(".tehachapi").empty().append($("input.tehachapi").val());
    $(".tonapa").empty().append($("input.tonapa").val());

    $(".rain").empty().append($("input.rain").val());
    $(".rig").empty().append($("input.rig").val());
    $(".made").empty().append($("input.made").val());
    $(".weighed").empty().append($("input.weighed").val());
    $(".weed").empty().append($("input.weed").val());

    $(".whites").empty().append($("input.whites").val());
    $(".wine").empty().append($("input.wine").val());
    $(".sign").empty().append($("input.sign").val());

    $(".wind").empty().append($("input.wind").val());
    $(".sleet").empty().append($("input.sleet").val());
    $(".stoved").empty().append($("input.stoved").val());
    $(".feet").empty().append($("input.feet").val());
    $(".mexico").empty().append($("input.mexico").val());
    $(".movin").empty().append($("input.movin").val());


    // show the story
    $("#story").show();

    // scroll down
    var n = $(document).height();
    $('html, body').animate({ scrollTop: 900 }, 500);

    // empty the form's values
    //$(':input').val('');

    // hide the questions
    //$("#questions").hide();

  });

  // ---- event handler ---- //
  $("#play-btn").click(function(e) {
    $("#questions").show();
    //$("#story").hide();
  });

});
